function [px, py] = grad(current_pos,delta,charges,target,reaction,rep,atr)

% funkcja wyliczajaca gradient z macierzy wartości potencjałów

px = -(field(current_pos(1,1)+delta,current_pos(1,2),charges,target,reaction,rep,atr)-field(current_pos(1,1),current_pos(1,2),charges,target,reaction,rep,atr))/delta;
py = -(field(current_pos(1,1),current_pos(1,2)+delta,charges,target,reaction,rep,atr)-field(current_pos(1,1),current_pos(1,2),charges,target,reaction,rep,atr))/delta;

% move_rob(current_pos,px,py);

end

