function msp(density,start_x,start_y,end_x,end_y,resolution,xmin,ymin,xmax,ymax,zmin,zmax,shift,reaction,delta,goal_delta,obstacles,rep,atr)



target(1,1) = end_x;
target(1,2) = end_y;

charges=make_charges(density,obstacles); 

% przygotowanie osi wykresu 3d
x(1)=xmin;
y(1)=ymin;

for i=1:resolution;
   x(i+1)=(((xmax-xmin)*(i))/resolution)+xmin;
end

for i=1:resolution+1;
   x(i,:)=x(1,:);
end

for i=1:resolution;
   y(i+1)=(((ymax-ymin)*(i))/resolution)+ymin;
end

for i=1:resolution+1;
   y(i,:)=y(1,:);
end
y=y';

% opbiczanie potencjałów na dla wykresu
for i=1:resolution+1
   for j=1:resolution+1;
       
    z(i,j)=field(x(i,j),y(i,j),charges,target,reaction,rep,atr);
    
    if z(i,j)>zmax
       z(i,j)=zmax; 
    end
    
      if z(i,j)<zmin
       z(i,j)=zmin; 
      end
    end
end

%wizualizacja potencjałów 3d

figure;
 surf(x,y,z);

current_pos = [start_x,start_y]; 
goal=0;
figure
plot(charges(:,1),charges(:,2),'.')
xlabel('x');
ylabel('y');
hold on;
plot(target(:,1),target(:,2),'.r')
plot(current_pos(:,1),current_pos(:,2),'.g');
pause(0.1)

while goal==0
    [px,py]=grad(current_pos,delta,charges,target,reaction,rep,atr);
    current_pos = move_rob(current_pos,px,py,shift);
    goal = if_goal(current_pos,target,goal_delta);
    plot(current_pos(:,1),current_pos(:,2),'.g');
    pause(0.1)
end

% testy
[px,py] = gradient(z);
px=-px;
py=-py;
scale=5;
figure
plot(charges(:,1),charges(:,2),'.')
xlabel('x');
ylabel('y');
hold on;
plot(target(:,1),target(:,2),'.r')
quiver(x,y,px,py,scale);

end