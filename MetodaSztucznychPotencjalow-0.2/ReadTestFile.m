function [ density, start_x, start_y, end_x, end_y, resolution, range_xmin, range_ymin,  range_xmax, range_ymax, range_zmin, range_zmax, shift, reaction, delta, goal_delta, scene,atr,rep ] = ReadTestFile( )
C= textread('test.txt', '%s','delimiter', '\n');
density = str2double(C{2});
%%
start_size=size(C{4});
for i=1:(start_size(2))
    if(C{4}(i)==' ')
        start_x= str2double(C{4}(1:i));
        start_y= str2double(C{4}(i:end));
    end
end
%%
end_size=size(C{6});
for i=1:(start_size(2))
    if(C{6}(i)==' ')
        end_x= str2double(C{6}(1:i));
        end_y= str2double(C{6}(i:end));
    end
end
%%
resolution = str2double(C{8});
%%
shift = str2double(C{10});
%%
reaction = str2double(C{12});
%%
rangex_size=size(C{15});
for i=1:(rangex_size(2))
    if(C{15}(i)==' ')
       range_xmin = str2double(C{15}(1:i));
       range_xmax = str2double(C{15}(i:end));
    end
end
%%
rangey_size=size(C{17});
for i=1:(rangey_size(2))
    if(C{17}(i)==' ')
       range_ymin = str2double(C{17}(1:i));
       range_ymax = str2double(C{17}(i:end));
    end
end
%%
rangez_size=size(C{19});
for i=1:(rangez_size(2))
    if(C{19}(i)==' ')
       range_zmin = str2double(C{19}(1:i));
       range_zmax = str2double(C{19}(i:end));
    end
end

delta = str2double(C{21});
goal_delta = str2double(C{23});
%%
atr=str2double(C{25});
rep=str2double(C{27});
%przeszkody
data_size= size(C);
count1 =1;
for k =29:data_size(1)
    scene_size=size(C{k});
    count2=1;
    start=1;
    for i=1:(scene_size(2))
    if(C{k}(i)==' ')
        scene(count1,count2)=str2double(C{k}(start:i));
        count2=count2+1;
        start=i+1;
        if count2==4
            scene(count1,count2)=str2double(C{k}(start:end));
            i=scene_size(2);
        end
    end
    end
    count1=count1+1;
end

end
